variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "avail_zone" {}

variable "vpc_name" {
  type = string
  default = "systtek-central"
}

variable "environment" {
  type = map
  default = {
    dev = "dev"
    pro = "pro"
  } 
}
variable "enable_dns_hostnames" {
  type    = bool
  default = true
}

variable "enable_nat_gateway" {
  type    = bool
  default = false
}

variable "one_nat_gateway_per_az" {
  type    = bool
  default = false
}

variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "single_nat_gateway" {
  type    = bool
  default = true
}