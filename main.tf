resource "aws_vpc" "dev-vpc" {
  cidr_block = var.vpc_cidr_block

  enable_dns_hostnames = var.enable_dns_hostnames

  tags = {
    Name = "${var.environment["dev"]}-${var.vpc_name}"
  }
}

resource "aws_subnet" "dev-priv-subnet-1" {
  vpc_id = aws_vpc.dev-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avail_zone

  tags = {
    Name = "${var.environment["dev"]}-${var.vpc_name}-subnet-1"
  }
}

